﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_manager
{
    public class CompetitorDbContext:DbContext
    {
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS; Database = CompetitionDB;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<CompetitorSkill> CompetitorSkills { get; set; }
        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<CompetitorSkill>().HasKey(cs => new { cs.CompetitorId, cs.SkillId });
            CompetitionInitializer.Seed(modelbuilder);
            
        }
    }
    
}
