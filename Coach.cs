﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_manager
{
    public class Coach
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int YearsExperience { get; set; }
        public int? CompetitorId { get; set; }
        public virtual Competitor Competitor { get; set; }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append($"ID: " + Id);
            str.Append($" Name: " + Name);
            str.Append($" Years of Experience: " + YearsExperience);
            str.Append($" CompetitorId: " + CompetitorId);

            return str.ToString();
        }
    }
    
}
