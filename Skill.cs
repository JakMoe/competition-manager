﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_manager
{
    public class Skill
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int Level { get; set; }
        public ICollection<CompetitorSkill> competitorSkills { get; set; }

    }
}
