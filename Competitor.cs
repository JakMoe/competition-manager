﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_manager
{
    public class Competitor
    {
       
        public int Id { get; set; }

        public string Name { get; set; }

        public string CombatBackground { get; set; }

        public string FightingStance { get; set; }

        public int Wins { get; set; }

        public int Defeats { get; set; }
        

    
        public Coach Coach { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public ICollection<CompetitorSkill> competitorSkills { get; set; }



        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append($"ID: " + Id);
            str.Append($" Name: " + Name);
            str.Append($" CombatBackground: " + CombatBackground);
            str.Append($" FightingStance: " + FightingStance);
            str.Append($" Record: " + Wins);
            str.Append($"-" + Defeats);
            return str.ToString();
        }
    }
}
