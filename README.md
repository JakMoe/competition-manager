# Competition manager

## Description
Console app where user can add or get list of all competitors,coaches,teams and
skills.

## Getting Started

Open it in Visual studio, hit run.
Follow instructions in app

## Authors

* **Jakob Hauge Moe** - *backend* - [JakMoe](https://gitlab.com/JakMoe)