﻿// <auto-generated />
using System;
using Competition_manager;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Competition_manager.Migrations
{
    [DbContext(typeof(CompetitorDbContext))]
    partial class CompetitorDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.2")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Competition_manager.Coach", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("CompetitorId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("YearsExperience")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CompetitorId")
                        .IsUnique()
                        .HasFilter("[CompetitorId] IS NOT NULL");

                    b.ToTable("Coaches");
                });

            modelBuilder.Entity("Competition_manager.Competitor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CombatBackground")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Defeats")
                        .HasColumnType("int");

                    b.Property<string>("FightingStance")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("TeamId")
                        .HasColumnType("int");

                    b.Property<int>("Wins")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TeamId");

                    b.ToTable("Competitors");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CombatBackground = "MMA",
                            Defeats = 69,
                            FightingStance = "ortodox",
                            Name = "Jakob Hauge Moe",
                            Wins = 69
                        },
                        new
                        {
                            Id = 2,
                            CombatBackground = "Street Fighter",
                            Defeats = 37,
                            FightingStance = "soutpaw",
                            Name = "Philip Aubert",
                            Wins = 13
                        },
                        new
                        {
                            Id = 3,
                            CombatBackground = "Verbal",
                            Defeats = 99,
                            FightingStance = "neither",
                            Name = "Donald Trump",
                            Wins = 0
                        },
                        new
                        {
                            Id = 4,
                            CombatBackground = "Wrestler",
                            Defeats = 3,
                            FightingStance = "ortodox",
                            Name = "Brock Lesnar",
                            Wins = 5
                        },
                        new
                        {
                            Id = 5,
                            CombatBackground = "BJJ",
                            Defeats = 9,
                            FightingStance = "southpaw",
                            Name = "Demian Maia",
                            Wins = 28
                        });
                });

            modelBuilder.Entity("Competition_manager.CompetitorSkill", b =>
                {
                    b.Property<int>("CompetitorId")
                        .HasColumnType("int");

                    b.Property<int>("SkillId")
                        .HasColumnType("int");

                    b.HasKey("CompetitorId", "SkillId");

                    b.HasIndex("SkillId");

                    b.ToTable("CompetitorSkills");
                });

            modelBuilder.Entity("Competition_manager.Skill", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Level")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Skills");
                });

            modelBuilder.Entity("Competition_manager.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Location")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Teams");
                });

            modelBuilder.Entity("Competition_manager.Coach", b =>
                {
                    b.HasOne("Competition_manager.Competitor", "Competitor")
                        .WithOne("Coach")
                        .HasForeignKey("Competition_manager.Coach", "CompetitorId");
                });

            modelBuilder.Entity("Competition_manager.Competitor", b =>
                {
                    b.HasOne("Competition_manager.Team", "Team")
                        .WithMany("Competitors")
                        .HasForeignKey("TeamId");
                });

            modelBuilder.Entity("Competition_manager.CompetitorSkill", b =>
                {
                    b.HasOne("Competition_manager.Competitor", "Competitor")
                        .WithMany("competitorSkills")
                        .HasForeignKey("CompetitorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Competition_manager.Skill", "Skill")
                        .WithMany("competitorSkills")
                        .HasForeignKey("SkillId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
