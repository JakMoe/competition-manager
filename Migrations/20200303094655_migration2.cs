﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Competition_manager.Migrations
{
    public partial class migration2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorSkill_Competitors_CompetitorId",
                table: "CompetitorSkill");

            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorSkill_Skills_SkillId",
                table: "CompetitorSkill");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompetitorSkill",
                table: "CompetitorSkill");

            migrationBuilder.RenameTable(
                name: "CompetitorSkill",
                newName: "CompetitorSkills");

            migrationBuilder.RenameIndex(
                name: "IX_CompetitorSkill_SkillId",
                table: "CompetitorSkills",
                newName: "IX_CompetitorSkills_SkillId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompetitorSkills",
                table: "CompetitorSkills",
                columns: new[] { "CompetitorId", "SkillId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorSkills_Competitors_CompetitorId",
                table: "CompetitorSkills",
                column: "CompetitorId",
                principalTable: "Competitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorSkills_Skills_SkillId",
                table: "CompetitorSkills",
                column: "SkillId",
                principalTable: "Skills",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorSkills_Competitors_CompetitorId",
                table: "CompetitorSkills");

            migrationBuilder.DropForeignKey(
                name: "FK_CompetitorSkills_Skills_SkillId",
                table: "CompetitorSkills");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompetitorSkills",
                table: "CompetitorSkills");

            migrationBuilder.RenameTable(
                name: "CompetitorSkills",
                newName: "CompetitorSkill");

            migrationBuilder.RenameIndex(
                name: "IX_CompetitorSkills_SkillId",
                table: "CompetitorSkill",
                newName: "IX_CompetitorSkill_SkillId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompetitorSkill",
                table: "CompetitorSkill",
                columns: new[] { "CompetitorId", "SkillId" });

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorSkill_Competitors_CompetitorId",
                table: "CompetitorSkill",
                column: "CompetitorId",
                principalTable: "Competitors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompetitorSkill_Skills_SkillId",
                table: "CompetitorSkill",
                column: "SkillId",
                principalTable: "Skills",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
