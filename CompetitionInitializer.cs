﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_manager
{
    class CompetitionInitializer
    {
        public static void Seed(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Competitor>().HasData(
                new Competitor { Id = 1, Name = "Jakob Hauge Moe", CombatBackground = "MMA", FightingStance = "ortodox", Wins = 69, Defeats = 69 },
                new Competitor { Id = 2, Name = "Philip Aubert", CombatBackground = "Street Fighter", FightingStance = "soutpaw", Wins = 13, Defeats = 37 },
                new Competitor { Id = 3, Name = "Donald Trump", CombatBackground = "Verbal", FightingStance = "neither", Wins = 0, Defeats = 99 },
                new Competitor { Id = 4, Name = "Brock Lesnar", CombatBackground = "Wrestler", FightingStance = "ortodox", Wins = 5, Defeats = 3 },
                new Competitor { Id = 5, Name = "Demian Maia", CombatBackground = "BJJ", FightingStance = "southpaw", Wins = 28, Defeats = 9 }
                );
        }
    }
}
