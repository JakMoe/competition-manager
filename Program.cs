﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Competition_manager
{
    class Program
    {
        public static void StartProgram()
        {
            Console.WriteLine("Welcome to this MMA competition manager!");
            UserChoice();
        }
        public static void UserChoice()
        {
            Console.WriteLine("What do you want to access? Coach,Team,Competitor or Skill?: ");
            string firstAnswer = Console.ReadLine().ToLower();
            if (firstAnswer == "competitor")
            {
                Console.WriteLine("Write Add to insert competitor \n" +
                    "Write Get to see all competitors \n" +
                    "Press X to exit");
                string answer = Console.ReadLine().ToLower();
                if (answer == "add")
                {
                    AddCompetitor();
                }
                else if (answer == "get")
                {
                    GetCompetitor();
                }
                else if (answer == "x")
                {
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine(answer + " is an invalid command please try again!");

                }
            }
            else if (firstAnswer == "coach")
            {
                Console.WriteLine("Write Add to insert coach \n" +
              "Write Get to see all coaches \n" +
              "Press X to exit");
                string answer = Console.ReadLine().ToLower();
                if (answer == "add")
                {
                    AddCoach();
                }
                else if (answer == "get")
                {
                    GetCoach();
                }
                else if (answer == "x")
                {
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine(answer + " is an invalid command please try again!");

                }
            }else if (firstAnswer == "team")
            {
                Console.WriteLine("Write Add to insert Team \n" +
         "Write Get to see all teams \n" +
         "Press X to exit");
                string answer = Console.ReadLine().ToLower();
                if (answer == "add")
                {
                    AddTeam();
                }
                else if (answer == "get")
                {
                    GetTeam();
                }
                else if (answer == "x")
                {
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine(answer + " is an invalid command please try again!");

                }

            }else if (firstAnswer == "skill")
            {
                Console.WriteLine("Write Create to create new skill  \n" +
                    "Write Add to link a skill to a competitor\n"+
         "Write Get to see all teams \n" +
         "Press X to exit");
                string answer = Console.ReadLine().ToLower();
                if (answer == "create")
                {
                   AddSkill();
                }
                else if (answer == "add")
                {
                    AddSkillToCompetitor();
                }
                else if (answer == "get")
                {
                    GetTeam();
                }
                else if (answer == "x")
                {
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine(answer + " is an invalid command please try again!");

                }

            }

        }

        public static void AddCompetitor()
        {
            Console.WriteLine("Name: ");
            string name = Console.ReadLine();
            Console.WriteLine("What is the competitors combat background?: ");
            string cb = Console.ReadLine();
            Console.WriteLine("Is the competitor fighting ortodox, southpaw or neither?:");
            string fs = Console.ReadLine();
            Console.WriteLine("How many wins?");
            int wins = Int32.Parse(Console.ReadLine());
            Console.WriteLine("How many defeats?");
            int defeats = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Does the competitor belong to a team? Y/N");
            string yesNo = Console.ReadLine().ToLower();
            if (yesNo == "y")
            {
                Console.WriteLine("Insert team ID: ");
                int teamId = Int32.Parse(Console.ReadLine());
                using (CompetitorDbContext competitorDbContext = new CompetitorDbContext())
                {
                    competitorDbContext.Competitors.Add(new Competitor { Name = name, CombatBackground = cb, FightingStance = fs, Wins = wins, Defeats = defeats, TeamId = teamId });
                    competitorDbContext.SaveChanges();
                }
            }
            else
            {
                using (CompetitorDbContext competitorDbContext = new CompetitorDbContext())
                {
                    competitorDbContext.Competitors.Add(new Competitor { Name = name, CombatBackground = cb, FightingStance = fs, Wins = wins, Defeats = defeats });
                    competitorDbContext.SaveChanges();
                }

            }
            UserChoice();
        }
        public static void AddCoach()
        {
            Console.WriteLine("Name: ");
            string name = Console.ReadLine();
            Console.WriteLine("How many years experience does the coach have?: ");
            int yearsExperience = Int32.Parse(Console.ReadLine());
            int competitorId = 0;
            Console.WriteLine("Does the the coach train a spesific competitor? Y/N");
            string yn = Console.ReadLine().ToLower();
            if (yn == "y")
            {
                Console.WriteLine("Put in competitor ID: ");
                competitorId = Int32.Parse(Console.ReadLine());
            }
            using (CompetitorDbContext competitorDbContext = new CompetitorDbContext())
            {

                if (competitorId == 0)
                {
                    competitorDbContext.Coaches.Add(new Coach { Name = name, YearsExperience = yearsExperience });
                    competitorDbContext.SaveChanges();
                }
                else
                {
                    competitorDbContext.Coaches.Add(new Coach { Name = name, YearsExperience = yearsExperience, CompetitorId = competitorId });
                    competitorDbContext.SaveChanges();
                }
            }

            UserChoice();
        }
        public static void AddTeam()
        {
            Console.WriteLine("Team name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Where is the team located?: ");
            string location = Console.ReadLine();
                using (CompetitorDbContext competitorDbContext = new CompetitorDbContext())
                {
                     
                    competitorDbContext.Teams.Add(new Team { Name = name, Location = location,});
                    competitorDbContext.SaveChanges();
                }
            UserChoice();
        }
        public static void AddSkill()
        {

            Console.WriteLine("Skill Name: ");
            string name = Console.ReadLine();
            Console.WriteLine("What level is the skill on a scale from 1-10?:  ");
            int level = Int32.Parse(Console.ReadLine());

            using (CompetitorDbContext competitorDbContext = new CompetitorDbContext())
            {
                competitorDbContext.Skills.Add(new Skill { Name = name, Level = level });
                competitorDbContext.SaveChanges();

            }
            UserChoice();
        }
        public static void AddSkillToCompetitor()
        {
            Console.WriteLine("Enter competitor ID: ");
            int compId = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter skill ID: ");
            int skillId = Int32.Parse(Console.ReadLine());
            using (CompetitorDbContext competitorDbContext = new CompetitorDbContext())
            {
                competitorDbContext.CompetitorSkills.Add(new CompetitorSkill { CompetitorId = compId, SkillId = skillId });
                competitorDbContext.SaveChanges();

            }
            UserChoice();
        }

        public static void GetCoach()
        {
            using (CompetitorDbContext comtext = new CompetitorDbContext())
            {
                foreach (Coach coach in comtext.Coaches)
                {
                    Console.WriteLine(coach.ToString());
                }
            }
        }
        public static void GetCompetitor()
        {
            using (CompetitorDbContext comtext = new CompetitorDbContext())
            {
                foreach (Competitor competitor in comtext.Competitors)
                {
                    Console.WriteLine(competitor.ToString());
                }
            }

        }
        public static void GetTeam()

        {
            using (CompetitorDbContext comtext = new CompetitorDbContext())
            {
                foreach (Team team in comtext.Teams)
                {
                    Console.WriteLine(team.ToString());
                }
            }

        }
        public static void GetSkill()
        {
            using (CompetitorDbContext comtext = new CompetitorDbContext())

                foreach (Skill skill in comtext.Skills)
            {
                Console.WriteLine(skill.ToString());
            }
        }
        static void Main(string[] args)
        {

            StartProgram();
           
        } 
    }
}
