﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Competition_manager
{
   public class Team
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Location { get; set; }

        public ICollection<Competitor> Competitors { get; set; }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.Append($"ID: " + Id);
            str.Append($" Name: " + Name);
            str.Append($" Location: " + Location);
            using (CompetitorDbContext competitorDbContext = new CompetitorDbContext())
            {
                CompetitorDbContext comtext = new CompetitorDbContext();
                List<Competitor> competiteritors = comtext.Competitors.Where(s => s.TeamId == Id).ToList();

                foreach (Competitor competitor in competiteritors)
                {
                    str.Append($"\n Competitor: " + competitor.Name);
                }
            }

            return str.ToString();
        }
    }
}
